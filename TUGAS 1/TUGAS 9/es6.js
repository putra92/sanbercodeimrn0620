//Soal no 1
var golden = () => {
  golden = "this is golden!!";
  return golden;
}
 
golden();

//Soal no 2
const firstName = 'Putranda'
const lastName = 'Erawan'
const name = `${firstName} ${lastName}`
 
console.log(name)

//Soal no 3
let newObject  = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
};
 
const { firstName, lastName, destination, occupation, spell } = newObject ;
 
console.log(firstName, lastName, destination, occupation, spell)

//Soal no 4
let west = ["Will", "Chris", "Sam", "Holly"];
let east = ["Gill", "Brian", "Noel", "Maggie"];

let combined = [...west.concat(east)];

console.log(combined);

//Soal no 5
const planet = "earth"
const view = "glass"
const teamName = 'Lorem ' + `${view}` + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + `${planet}` + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
console.log(teamName)