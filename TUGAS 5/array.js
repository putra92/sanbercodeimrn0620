//Soal no 1
	function range(a, b){
	  var hasil = [];
	  if(a == null || b == null){
		hasil.push(-1);
	  }
	  else if(a<b)
	  {
		while(a<=b){
		  hasil.push(a);
		  a++;
		}
	  } 
	  else if(a>b)
	  {
		while(a>=b){
		  hasil.push(a);
		  a--;
		}
	  }
	  return hasil;
	}


	console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	console.log(range(1)) // -1
	console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
	console.log(range(54, 50)) // [54, 53, 52, 51, 50]
	console.log(range()) // -1 
	
// Soal no 2
	function rangeWithStep(a, b, c){
	  var hasil = [];
	  if(a>b)
	  {
		while(a>=b){
		  hasil.push(a);
		  a-=c;
		}
	  } 
	  else if(a<b)
	  {
		while(a<=b){
		  hasil.push(a);
		  a+=c;
		}
	  }
	  return hasil;
	}


	console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
	console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
	console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
	console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
	
// soal no 3
	function sum(a, b, c){
		  var hasil = [];
		var total = 0;
		if(a==null)
		{
		  hasil.push(0);
		}
		else if(b==null)
		{
		  hasil.push(a);
		}
		else if(c==null)
		{
		  c=1;
		}
		  
		if(a>b)
		  {
			while(a>=b){
			  hasil.push(a);
			  a-=c;
			}
		  } 
		  else if(a<b)
		  {
			while(a<=b){
			  hasil.push(a);
			  a+=c;
			}
		  }

		for(var i = 0; i < hasil.length; i++)
		{
		 total = total + hasil[i]; 
		}
		  return total;
		}


	console.log(sum(1,10)) // 55
	console.log(sum(5, 50, 2)) // 621
	console.log(sum(15,10)) // 75
	console.log(sum(20, 10, 2)) // 90
	console.log(sum(1)) // 1
	console.log(sum()) // 0 	
	
// Soal no 4
	   var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 
		function dataHandling (){
			var string = ["Nomor ID: ", "Nama Lengkap: ", "TTL: ", " ", "Hobi: "];
			var hasil = "";
			var hasil_row = "";

			for(i=0;i<input.length;i++)
			{
			  for(j=0;j<input[0].length;j++)
			  {
				hasil_row = string[j] + input[i][j];
				if(j==2){
				  hasil += hasil_row;
				}else if(j==input[0].length-1){
				  hasil += hasil_row + '\n\n';
				}else{
				  hasil += hasil_row + '\n';
				}
			  }
			}
			  return hasil;
			}

		console.log(dataHandling());
		
// Soal no 5
	function balikKata(str) {
	 var currentString = str;
	 var newString = '';for (let i = str.length - 1; i >= 0; i--) {
	  newString = newString + currentString[i];
	 }
	 
	 return newString;
	}

	console.log(balikKata("Kasur Rusak")) // kasuR rusaK
	console.log(balikKata("SanberCode")) // edoCrebnaS
	console.log(balikKata("Haji Ijah")) // hajI ijaH
	console.log(balikKata("racecar")) // racecar
	console.log(balikKata("I am Sanbers")) // srebnaS ma I 
	
// Soal no 6
	var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];

	function dataHandling2 (a){
		var hasil = "";
		a[1].concat("Elsharawy");
		a.splice(1, 5, a[1].concat("Elsharawy"), "Provinsi "+a[2], a[3], "Pria", "SMA Internasional Metro");
		hasil = a;
		var tgl = a[3].split("/");
		var bulan = tgl[1];
		var tgls = a[3].split("/");
		var nama = a[1];
		var tglss = tgls.join("-");

		switch(bulan) {
		  case "01": 
			bulan="Januari"; 
			break;
		  case "02":   
			bulan="Februari"; 
			break; 
		  case "03":   
			bulan="Maret"; 
			break; 
		  case "04":   
			bulan="April"; 
			break;
		  case "05":   
			bulan="Mei"; 
			break;
		}

		console.log(hasil);
		console.log(bulan);
		console.log(tgls);
		console.log(tglss);
		console.log(nama);
		return "";
		}

	console.log(dataHandling2(input));