// Soal A
	function balikString(str) {
	 var currentString = str;
	 var newString = '';for (let i = str.length - 1; i >= 0; i--) {
	  newString = newString + currentString[i];
	 }
	 
	 return newString;
	}

	console.log(balikString("abcde")) // edcba
	console.log(balikString("rusak")) // kasur
	console.log(balikString("racecar")) // racecar
	console.log(balikString("haji")) // ijah
	
// Soal B
	function palindrome(str) {
	  var len = Math.floor(str.length / 2);
	  for (var i = 0; i < len; i++)
		if (str[i] !== str[str.length - i - 1])
		  return false;
	  return true;
	}

	console.log(palindrome("kasur rusak")) // true
	console.log(palindrome("haji ijah")) // true
	console.log(palindrome("nabasan")) // false
	console.log(palindrome("nababan")) // true
	console.log(palindrome("jakarta")) // false
	
// Soal C
	function bandingkan(a, b){
	  var hasil = [];
	  if(a == b || a < 0 || a == null || b == null)
		{
			  hasil.push(-1);
		  }
		else if (a < b)
		{
		  hasil.push(b);
		}
		  return hasil;
		}


	console.log(bandingkan(10, 15)); // 15
	console.log(bandingkan(12, 12)); // -1
	console.log(bandingkan(-1, 10)); // -1 
	console.log(bandingkan(112, 121));// 121
	console.log(bandingkan(1)); // 1
	console.log(bandingkan()); // -1
	console.log(bandingkan("15", "18")) // 18
