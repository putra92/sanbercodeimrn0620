// Soal A
	function AscendingTen(a){
	  var input  = [];
	  if ( a == null)
    {
      input.push(-1);
    }else{
      if(a == 11){
        for(i=a; i < 21; i++)
        {
          input.push(i);
        }
      }
      else
      {
        for(i=a; i < 31; i++)
        {
          input.push(i);
        }
      }
    }
	  return input;
	}

	console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
	console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
	console.log(AscendingTen()) // -1
	
	
// Soal B
	function DescendingTen(a){
	  var input  = [];
	  if ( a == null)
    {
      input.push(-1);
    }else{
      if(a == 100){
        for(i=a; i > 90; i--)
        {
          input.push(i);
        }
      }
      else
      {
        for(i=a; i > 0; i--)
        {
          input.push(i);
        }
      }
    }
	  return input;
	}

	console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
	console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
	console.log(DescendingTen()) // -1