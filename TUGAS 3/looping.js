// Soal 1
	var flag = 2;
	console.log('Tugas Pertama');
	while(flag < 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
	  console.log('Iterasi ke-' + flag); // Menampilkan nilai flag pada iterasi tertentu
	  flag+=2; // Mengubah nilai flag dengan menambahkan 1
	}
	console.log('-----------------------------');
	console.log('Tugas kedua');
	while(flag >= 2) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
	  console.log('Iterasi ke-' + flag); // Menampilkan nilai flag pada iterasi tertentu
	  flag-=2; // Mengubah nilai flag dengan menambahkan 1
	}

// Soal 2
    var nilai = 1;
   		var akhir = 20;
       while( nilai <= akhir ){
      if(nilai%3==0)
      {
        console.log(nilai+"- I Love Coding" );
      }
   		else if(nilai%2==0)
   		{
   			console.log(nilai+"- Berkualitas" );
   		} else
   		{
   			console.log(nilai+"- Santai" );
   		} 
   		nilai++;
   	}
	
// Soal 3
	let pagar = '';

	for(let i = 0; i < 4; i++){
	  for(let j = 0; j < 8; j++){
		pagar += '#';
	  }
		pagar += '\n';
	}

	console.log(pagar);
	
// Soal 4
	  let piramid = '';

		for(let i = 0; i <= 7; i++){
		  for(let j = 0; j <= i; j++){
			piramid += '#';
		  }
			piramid += '\n';
		}
	  console.log(piramid);
	  
// Soal 5
    var nilai = 1;
   		var akhir = 8;
       while( nilai <= akhir ){
      if(nilai%2==0)
   		{
   			console.log("# # # # # # # #" );
   		} else
   		{
   			console.log(" # # # # # # # #" );
   		} 
   		nilai++;
   	}